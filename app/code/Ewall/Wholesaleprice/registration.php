<?php
 
use \Magento\Framework\Component\ComponentRegistrar;
 
ComponentRegistrar::register(
	ComponentRegistrar::MODULE, 
	'Ewall_Wholesaleprice', 
	__DIR__);