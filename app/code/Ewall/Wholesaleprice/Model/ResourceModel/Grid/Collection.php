<?php 
namespace Ewall\Wholesaleprice\Model\ResourceModel\Grid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'wholesale_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ewall\Wholesaleprice\Model\Grid', 'Ewall\Wholesaleprice\Model\ResourceModel\Grid');
    }

}