<?php
namespace Ewall\Wholesaleprice\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session as CustomerModelSession;
use Magento\Framework\Event\Observer as EventObserver;


class ProcessAdminFinalPriceObserver implements ObserverInterface
{ protected $_modelGridFactory;
  protected $_productCollectionFactory;
  protected $_storeManager;
  protected $_customerSession;

  public function __construct(
    \Ewall\Wholesaleprice\Model\GridFactory $gridFactory,
     ProductCollectionFactory $productCollectionFactory,
     StoreManagerInterface $storeManager,CustomerModelSession $customerSession
        ){
      $this->_modelGridFactory = $gridFactory;
      $this->_productCollectionFactory = $productCollectionFactory;
      $this->_storeManager = $storeManager;
      $this->_customerSession = $customerSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       $collection = $observer->getEvent()->getCollection();
       if ($observer->hasCustomerGroupId()) {
        $gId = $observer->getEvent()->getCustomerGroupId();

       }elseif ($product->hasCustomerGroupId()) {
            $gId = $product->getCustomerGroupId();
      } else {
          $gId = $this->_customerSession->getCustomerGroupId();
          //echo "<pre>";print_r($gId);exit();
      }
      $cgid = $gId . ',';
      //echo "<pre>";print_r($cgid);exit();
      $collection = $this->_modelGridFactory->create()->getCollection()->addFieldToFilter('status', 1);
        $collection->addFieldToFilter('customer_group_id', array('like' => '%,' . $cgid . '%', 'like' => $cgid . '%'));
       foreach ($collection as $rule) {
         if ($rule->getWebsiteid() == $this->_storeManager->getStore()->getWebsiteId()) {
              if ($rule->getStoreid() == $this->_storeManager->getStore()->getId() || $rule->getStoreid()== 0) {
                $arrfun[] = $rule->getData();
             }
          }
       }   //echo "<pre>";print_r($rule->getData());exit;
       $collection->addsort('priority','ASC');

      //  if (!empty($arrfun)) {
      //     $minArray = array();
      //     foreach ($arrfun as $values) {
      //        $minArray[$values['priority']] = $values['wholesale_id'];
      //     }
      // }


       $rule_id = $minArray[min(array_keys($minArray)) ];
        if ($rule_id) {
            $collection = $this->_modelGridFactory->create()->getCollection()->addFieldToFilter('status', 1)->addFieldToFilter('wholesale_id', $rule_id);
            //echo "<pre>";print_r($collection->getData());exit();
            foreach ($collection as $rule) {
                $rule_id = $rule->getId();
                $customerGroupId = $rule->getCustomerGroupId();
            }
        $customerGroupId = (array)explode(',', $customerGroupId, -1);
            if (in_array($gId, $customerGroupId)) {
               foreach ($collection as $product) {
                    $type = $product->getData('type_id');
                $collection = $this->_productCollectionFactory->create()->getCollection();
                $collection->addFieldToFilter('rule_id',$rule_id)->addFieldToFilter('product_id',$product_id)->load();
                foreach ($collection as $prod) {
                  $product ->setPrice($prod->getRulePrice());
                }
            }
        return $this;
    }
 }
}
}