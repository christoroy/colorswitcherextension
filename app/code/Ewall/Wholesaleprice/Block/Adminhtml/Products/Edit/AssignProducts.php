<?php
namespace Ewall\Wholesaleprice\Block\Adminhtml\Products\Edit;

use Magento\TestFramework\ObjectManager;

    class AssignProducts extends \Magento\Backend\Block\Template
    {
        /**
         * Block template
         *
         * @var string
         */
        protected $_template = 'products/assign_products.phtml';

        /**
         * @var \Magento\Catalog\Block\Adminhtml\Category\Tab\Product
         */
        protected $blockGrid;

        /**
         * @var \Magento\Framework\Registry
         */
        protected $registry;

        /**
         * @var \Magento\Framework\Json\EncoderInterface
         */
        protected $jsonEncoder;

        protected $collection;


        protected $_productCollectionFactory;

        /**
         * AssignProducts constructor.
         *
         * @param \Magento\Backend\Block\Template\Context $context
         * @param \Magento\Framework\Registry $registry
         * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
         * @param array $data
         */
        public function __construct(
            \Magento\Backend\Block\Template\Context $context,
            \Magento\Framework\Registry $registry,
            \Magento\Framework\Json\EncoderInterface $jsonEncoder,
            \Ewall\Wholesaleprice\Model\Grid $collection,
            \Ewall\Wholesaleprice\Model\ResourceModel\Grid\CollectionFactory $productCollectionFactory, //your custom collection
            array $data = []
        ) {
            $this->registry = $registry;
            $this->jsonEncoder = $jsonEncoder;
            $this->_collection = $collection;
            $this->_productCollectionFactory = $productCollectionFactory;
            parent::__construct($context, $data);
        }

        /**
         * Retrieve instance of grid block
         *
         * @return \Magento\Framework\View\Element\BlockInterface
         * @throws \Magento\Framework\Exception\LocalizedException
         */
        public function getBlockGrid()
        {

            if (null === $this->blockGrid) {
                $this->blockGrid = $this->getLayout()->createBlock(
                    'Ewall\Wholesaleprice\Block\Adminhtml\Products\Edit\Tab\Product',
                    'category.product.grid'
                );
            }
            return $this->blockGrid;
        }

        /**
         * Return HTML of grid block
         *
         * @return string
         */
        public function getGridHtml()
        {

            return $this->getBlockGrid()->toHtml();
        }

        /**
         * @return string
         */
        public function getProductsJson()
        {

            $wholesale_id = $this->getRequest()->getParam('wholesale_id');
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $collections = $this->_collection->load($wholesale_id);
            // $collection_products = array_fill_keys(explode(',', $this->_collection->getData('assign_products')), "");
            $collection_products = unserialize($collections->getData('assign_products'));

            if ($wholesale_id) {
                return $this->jsonEncoder->encode($collection_products);
            }
            return '{}';

            // $vProducts = $this->_productCollectionFactory->create();
            // $products = array();
            // foreach($vProducts as $pdct){
            //     $products[$pdct->getProductId()]  = '';
            // }

            // if (!empty($products)) {
            //     return $this->jsonEncoder->encode($products);
            // }
            // return '{}';
        }
        public function getGrid()
        {
            return $this->registry->registry('grid');
        }
    }