<?php
namespace Ewall\Wholesaleprice\Controller\Adminhtml\Grid;
 
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
 
class Index extends \Magento\Backend\App\Action
{
  const ADMIN_RESOURCE = 'Ewall_Wholesaleprice::grid';
 
  /**
  * @var PageFactory
  */
  protected $resultPageFactory;
 
  /**
  * @param Context $context
  * @param PageFactory $resultPageFactory
  */
  public function __construct(Context $context,PageFactory $resultPageFactory) {
    parent::__construct($context);
    $this->resultPageFactory = $resultPageFactory;
  }
 
  /**
  * Index action
  *
  * @return \Magento\Backend\Model\View\Result\Page
  */
  public function execute()
  {
    /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
    $resultPage = $this->resultPageFactory->create();
    $resultPage->setActiveMenu('Ewall_Wholesaleprice::grid');
    $resultPage->addBreadcrumb(__('Manage Wholesale Price Rule'), __('Manage Wholesale Price Rule'));
    $resultPage->addBreadcrumb(__('Manage Wholesale Price Rule'), __('Manage Wholesale Price Rule'));
    $resultPage->getConfig()->getTitle()->prepend(__('Wholesale Price Rules'));
 
    return $resultPage;
  }
 
  /**
  * {@inheritdoc}
  */
  protected function _isAllowed()
  {
    return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
  }
}