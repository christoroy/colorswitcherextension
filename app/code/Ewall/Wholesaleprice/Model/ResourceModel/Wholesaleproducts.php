<?php

namespace Ewall\Wholesaleprice\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
class Wholesaleproducts extends  AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        /* Custom Table Name */
         $this->_init('ewall_wholesaleproducts','entity_id');
    }
}

?>