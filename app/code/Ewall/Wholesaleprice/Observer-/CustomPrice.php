<?php
/**
* Ewall Wholesaleprice CustomPrice Observer
*
* @category    Ewall
* @package     Ewall_Wholesaleprice
* @author      Ewall Software Private Limited
*
*/
namespace Ewall\Wholesaleprice\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class CustomPrice implements ObserverInterface
{
  protected $_modelGridFactory;

  protected $_customerSession;

  protected $_storeManager;

  protected $_productCollectionFactory;

  protected $_categoryFactory;

  protected $_helper;

  public function __construct(
    \Ewall\Wholesaleprice\Model\GridFactory $gridFactory,
    \Ewall\Wholesaleprice\Helper\Data $helper,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Customer\Model\Session $customerSession,
    \Magento\Catalog\Model\CategoryFactory $categoryFactory,
    \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_modelGridFactory = $gridFactory;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_helper = $helper;
      }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
	$gridCollection = $this->_modelGridFactory->create()->getCollection();
	foreach ($gridCollection->setOrder('priority','DESC') as $gridData) {
	    if ($status= $gridData->getStatus()) {
	    // echo "string";exit;
	        $store_id = $gridData->getStoreid();
	        $website_id = $gridData->getWebsiteid();
	        $customer_group = explode(',', $gridData->getCustomerGroupid());
	        $priority = $gridData->getPriority();
	        $general_discount = $gridData->getDiscount();
	        $category_discount = $gridData->getCategoryDiscount();
	        // $current_categories = unserialize($gridData->getCategoryIds());
	        // $assign_products = unserialize($gridData->getAssignProducts());
	        $current_categories = unserialize($gridData->getCategoryIds()) ? $current_categories = unserialize($gridData->getCategoryIds()) : $current_categories = null;
        	$assign_products = unserialize($gridData->getAssignProducts()) ? $assign_products = unserialize($gridData->getAssignProducts()) : $assign_products = null;
	    }else {
	        $store_id = null;
	        $website_id = null;
	        $customer_group = null;
	      	$general_discount = null;
	        $category_discount = null;
	        $current_categories = null;
	        $assign_products = null;
	        $priority = null;
	    }
	}

    $current_customer_group = $this->_customerSession->getCustomer()->getGroupId();
    $current_store_id = $this->_storeManager->getStore()->getId();
    $current_website_id = $this->_storeManager->getStore()->getWebsiteId();
    $wholesaleprice_cofig_status = $this->_helper->getConfigValue('wholesaleprice/general/enable');

    if ($wholesaleprice_cofig_status == '1' && isset($priority) && in_array($current_customer_group, $customer_group) && $current_store_id == $store_id && $current_website_id == $website_id) {
    	isset($assign_products) ? $assign_product_id = array_keys($assign_products) : $assign_product_id = array();;
	    $product = $observer->getEvent()->getProduct();
	    $current_product_id = $product->getData('entity_id');
	    // print_r($assign_product_id);exit;
	    // print_r($current_product_id);exit;
	    if (isset($current_categories)) {
	      foreach ($current_categories as $categoryData) {
	        $category = $this->_categoryFactory->create()->load($categoryData);
	        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoryFilter($category)->load()->getData();
	        foreach ($collection as $value) {
	          // print_r($value); exit;
	          $category_product_ids[] = $value['entity_id'];
	        }
	      }
	    }
  	// echo "string";exit;

	    if (in_array($current_product_id, $assign_product_id)){
	      $cost = $product->load($current_product_id)->getData('price');
	      $discount_price = $cost-round(($cost / 100) * $assign_products[$current_product_id]);
	      $item = $observer->getEvent()->getData('quote_item');
	      $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
	      $price = $discount_price; //set your price here
	      $item->setCustomPrice($price);
	      $item->setOriginalCustomPrice($price);
	      $item->getProduct()->setIsSuperMode(true);
	    }elseif (in_array($current_product_id, $category_product_ids)) {
	      $cost = $product->load($current_product_id)->getData('price');
	      $discount_price = $cost-round(($cost / 100) * $category_discount);
	      $item = $observer->getEvent()->getData('quote_item');
	      $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
	      $price = $discount_price; //set your price here
	      $item->setCustomPrice($price);
	      $item->setOriginalCustomPrice($price);
	      $item->getProduct()->setIsSuperMode(true);
	    }else{
	      $cost = $product->load($current_product_id)->getData('price');
	      $discount_price = $cost-round(($cost / 100) * $general_discount);
	      $item = $observer->getEvent()->getData('quote_item');
	      $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
	      $price = $discount_price; //set your price here
	      $item->setCustomPrice($price);
	      $item->setOriginalCustomPrice($price);
	      $item->getProduct()->setIsSuperMode(true);
	    }
    }

  }
}