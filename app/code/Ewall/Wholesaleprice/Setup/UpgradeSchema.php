<?php
namespace Ewall\Wholesaleprice\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,ModuleContextInterface $context){
        $installer = $setup;
        if(version_compare($context->getVersion(), '2.0.1') < 0) {
            $transactionTable = $installer->getConnection()
            ->newTable($installer->getTable('ewall_wholesaleproducts'))
                ->addColumn(
                    'product_id',
                     \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                     null,
                     [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Product_Id'
                   )
                ->addColumn(
                    'rule_id',
                     \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                     null,
                     [
                        'unsigned' => true,
                        'nullable' => false,
                    ],
                    'Rule_Id'
                    )
                ->addColumn(
                    'discount',
                     \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                     null,
                     [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Discount'
                    );
            $installer->getConnection()->createTable($transactionTable);
        }
        $installer->endSetup();
    }
}
