<?php
 
namespace Ewall\Wholesaleprice\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Ewall\Wholesaleprice\Model\GridFactory;
 
class Index extends Action
{
    /**
     * @var \Tutorial\SimpleNews\Model\NewsFactory
     */
    protected $_modelGridFactory;
 
    /**
     * @param Context $context
     * @param NewsFactory $modelNewsFactory
     */
    public function __construct(
        Context $context,
        GridFactory $modelGridFactory
    ) {
        parent::__construct($context);
        $this->_modelGridFactory = $_modelGridFactory;
    }
 
    public function execute()
    {
        /**
         * When Magento get your model, it will generate a Factory class
         * for your model at var/generaton folder and we can get your
         * model by this way
         */
        $gridModel = $this->_modelGridFactory->create();

        // Load the item with ID is 1
        $item = $gridModel->load(1);
        //var_dump($item->getData());
 
        // Get news collection
        $gridCollection = $gridModel->getCollection();
        echo "<pre>";print_r($gridCollection->getData());exit;
        // Load all data of collection
        var_dump($gridCollection->getData());
    }
}