<?php

namespace Ewall\Wholesaleprice\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // Get Ewall_Wholesale table
        $tableName = $installer->getTable('ewall_wholesalepricerule');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            // Create Ewall_Wholesale table
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'wholesale_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'rulename',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'RuleName'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Status'
                )
                ->addColumn(
                    'websiteid',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false],
                    'Websiteid'
                    )
                ->addColumn(
                    'storeid',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false],
                    'Storeid'
                    )
                ->addColumn(
                    'customer_groupid',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Customer_Groupid'
                )
                ->addColumn(
                    'discount',
                    Table::TYPE_DECIMAL,
                    null,
                    ['nullable' => false],
                    'Discount'
                    )
                ->addColumn(
                    'category_discount',
                    Table::TYPE_DECIMAL,
                    null,
                    ['nullable' => false],
                    'Category Discount'
                    )
                ->addColumn(
                    'priority',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false],
                    'Priority'
                    )
                ->addColumn(
                    'category_ids',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Category_ids'
                    )
                ->addColumn(
                    'products',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Products'
                    )
                ->addColumn(
                    'created_time',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'CreatedTime'
                    )
                 ->addColumn(
                    'updated_time',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'UpdatedTime'
                    )
                ->addColumn(
                    'categories',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default'=>''],
                    'Categories'
                    )
                ->addColumn(
                    'assign_products',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default'=>''],
                    'AssignProducts'
                    )
                ->setComment('Wholesaleprice Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $transactionTable = $installer->getConnection()
            ->newTable($installer->getTable('ewall_wholesaleproducts'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Entity_ID'
               )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Product_Id'
                )
            ->addColumn(
                'rule_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                ],
                'Rule_Id'
                )
            ->addColumn(
                'discount',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false
                ],
                'Discount'
                );
        $installer->getConnection()->createTable($transactionTable);

        $installer->endSetup();
    }
}


