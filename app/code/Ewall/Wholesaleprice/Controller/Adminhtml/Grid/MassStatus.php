<?php
namespace Ewall\Wholesaleprice\Controller\Adminhtml\Grid;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Ewall\Wholesaleprice\Model\ResourceModel\Grid\CollectionFactory;

class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter.
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param Context           $context
     * @param Filter            $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    )
    {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $status = (int) $this->getRequest()->getParam('status');
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $recordUpdated = 0;
        foreach ($collection->getItems() as $auctionProduct) {
            $auctionProduct->setId($auctionProduct->getWholesaleId());
            $auctionProduct->setStatus($status);
            $auctionProduct->save();
            $recordUpdated++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 records have been Updated.', $recordUpdated)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }
}
