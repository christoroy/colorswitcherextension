<?php
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */

namespace Ewall\Wholesaleprice\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    protected $collection;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Ewall\Wholesaleprice\Model\Grid $collection,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->_collection = $collection;
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ewall_Wholesaleprice::save');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ewall_Wholesaleprice::grid')
            ->addBreadcrumb(__('Staff'), __('Staff'))
            ->addBreadcrumb(__('General Infomation'), __('General Infomation'));
        return $resultPage;
    }

    /**
     * Edit Staff
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->_initItem();
        $id = $this->getRequest()->getParam('wholesale_id');
        /** @var \Magebuzz\Staff\Model\Grid $this->_collection */
        // $this->_collection = $this->_objectManager->create('Ewall\Wholesaleprice\Model\Grid');
        if ($id) {
            $this->_collection->load($id);
            // print_r($this->_collection->getData());exit;
            if (!$this->_collection->getId()) {
                $this->messageManager->addError(__('This wholesaleprice no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $this->_collection->setData($data);
        }

        $this->_coreRegistry->register('wholesaleprice_grid', $this->_collection);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Grid') : __('New Grid'),
            $id ? __('Edit Grid') : __('New Grid')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Wholesaleprice Grid'));
        $resultPage->getConfig()->getTitle()
            ->prepend($this->_collection->getId() ? $this->_collection->getTitle() : __('Add Rule'));

        return $resultPage;
    }

    protected function _initItem($getRootInstead = false)
    {
        $id = (int)$this->getRequest()->getParam('wholesale_id', false);
        $myModel = $this->_objectManager->create('Ewall\Wholesaleprice\Model\Grid');

        if ($id) {
            $myModel->load($id);
        }
        $this->_objectManager->get('Magento\Framework\Registry')->unregister('grid');
        $this->_objectManager->get('Magento\Framework\Registry')->register('grid', $myModel);
        $this->_objectManager->get('Magento\Cms\Model\Wysiwyg\Config');
        return $myModel;
    }
}