<?php
namespace Ewall\Wholesaleprice\Block\Adminhtml\Products\Edit\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;

class Product extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $logger;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    protected $collection;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    protected $_resource;

    protected $_productCollectionFactory;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Ewall\Wholesaleprice\Model\ResourceModel\Grid\CollectionFactory $productCollectionFactory,
        \Ewall\Wholesaleprice\Model\Grid $collection,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resource = $resource;
        $this->_collection = $collection;
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('catalog_category_products');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setDefaultFilter(['in_product' => 1]);
        $this->setUseAjax(true);
    }
    public function getGrid()
    {
        return $this->_coreRegistry->registry('grid');
    }

    /**
     * @param Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_product') {
            $ids = $this->_getSelectedProducts();
            if (empty($ids)) {
                $ids = 0;
            }
            if ($column->getFilter()->getValue() == '1') {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$ids));
            } elseif ($column->getFilter()->getValue() == '0') {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$ids));
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        // if ($this->getGrid()->getId()) {
        //     $this->setDefaultFilter(['in_product' => 1]);
        // }
        $collection = $this->_productFactory->create()->getCollection()->addAttributeToSelect(
            'name'
        )->addAttributeToSelect(
            'sku'
        )->addAttributeToSelect(
            'price'
            );
        $rule_id = $this->getRequest()->getParam('wholesale_id');
        if (empty($rule_id)) {
        	$rule_id ='0';
        }

        $connection = $this->_resource->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION');
		$tableName = $connection->getTableName('ewall_wholesaleproducts');
		$collection->getSelect()->joinLeft(array('second' => $tableName), "e.entity_id = second.product_id and rule_id = $rule_id", ['discount']);
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        if ($storeId > 0) {
            $collection->addStoreFilter($storeId);
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_product',
            [
                'type' => 'checkbox',
                'name' => 'in_product',
                'values' => $this->_getSelectedProducts(),
                'index' => 'entity_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                // 'sortable' => true,
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'name', [
                'header' => __('Name'),
                'index' => 'name'
            ]
        );
        $this->addColumn(
            'type_id',
            [
                'header' => __('Type'),
                'index' => 'type_id',
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku'
             ]
        );
       // $this->addColumn(
       //      'visibility',
       //      [
       //          'header' => __('Visibility'),
       //          'index' => 'visibility',
       //          'type' => 'options',
       //          //'options' => $this->_visibility->getOptionArray(),
       //          'header_css_class' => 'col-visibility',
       //          'column_css_class' => 'col-visibility'
       //      ]
       //  );
        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'currency_code' => (string)$this->_scopeConfig->getValue(
                    \Magento\Directory\Model\Currency::XML_PATH_CURRENCY_BASE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                'index' => 'price'
            ]
        );
      $this->addColumn(
            'discount',
            [
                'header' =>__('Discount in %'),
                'name' => 'discount',
                'type' => 'input',
                'validate_class' => 'validate-number',
                'index'  => 'discount',
                'editable' => true,
                'edit_only' => true,
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('wholesaleprice/products/grid', ['_current' => true, 'collapse' => null]);
    }

    /**
     * @return array
     */
    protected function _getSelectedProducts()
    {
        $productIds = array();
        $wholesale_id = $this->getRequest()->getParam('wholesale_id');
        if ($wholesale_id) {
            $collection = $this->_productCollectionFactory->create()->load()->addFieldToFilter('wholesale_id', array('eq' => $wholesale_id))->getData();
            foreach ($collection as $current_collection) {
                $assignproducts = $current_collection['assign_products'];
            }
            if($assignproducts !== null){
                $products = unserialize($assignproducts);
                $productIds = array_keys($products);
            }
        }
        return $productIds;
    }

}