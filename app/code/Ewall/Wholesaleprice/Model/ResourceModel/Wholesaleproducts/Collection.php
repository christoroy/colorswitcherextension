<?php

namespace Ewall\Wholesaleprice\Model\ResourceModel\Wholesaleproducts;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
class Collection extends AbstractCollection
{
	protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init('Ewall\Wholesaleprice\Model\Wholesaleproducts', 'Ewall\Wholesaleprice\Model\ResourceModel\Wholesaleproducts');
    }
}

?>