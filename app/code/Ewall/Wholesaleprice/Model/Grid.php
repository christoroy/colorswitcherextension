<?php
namespace Ewall\Wholesaleprice\Model;

use Magento\Framework\DataObject\IdentityInterface;


class Grid  extends \Magento\Framework\Model\AbstractModel implements IdentityInterface{
  /**#@+
  * Grid's Statuses
  */
  const STATUS_ENABLED     = 1;
  const STATUS_DISABLED    = 0;
  const WHOLESALE_ID       = 'wholesale_id';
  const RULENAME           ='rulename';
  const STATUS             = 'status';
  const ASSIGNPRODUCTS     = 'assign_products';
  const DISCOUNT           ='discount';
  const PRIORITY           ='priority';
  const WEBSITEID          ='websiteid';
  const STOREID            ='storeid';
  const CUSTOMER_GROUPID   ='customer_groupid';
  const PRODUCT_ID         ='product_id';
    /**#@-*/

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'wholesaleprice_grid';

    /**
     * @var string
     */
    protected $_cacheTag = 'wholesaleprice_grid';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'wholesaleprice_grid';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct(){
        $this->_init('Ewall\Wholesaleprice\Model\ResourceModel\Grid');
    }


    /**
     * Prepare post's statuses.
     * Available event blog_post_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses(){
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
    /**
     * Get ID
     *
     * @return int|null
     */
     public function getIdentities(){
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    public function getWholesaleId(){
        return $this->getData(self::WHOLESALE_ID);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getRuleName(){
        return $this->getData(self::RULENAME);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getStatus(){
        return $this->getData(self::STATUS);
    }

     public function getAssignProducts(){
        return $this->getData(self::ASSIGNPRODUCTS);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
     public function getDiscount(){
        return $this->getData(self::DISCOUNT);
    }
     public function getPriority(){
        return $this->getData(self::PRIORITY);
    }
     public function getWebsiteId(){
        return $this->getData(self::WEBSITEID);
    }
     public function getStoreId(){
        return $this->getData(self::STOREID);
    }
     public function getCustomerGroupId(){
        return $this->getData(self::CUSTOMER_GROUPID);
    }

    public function setWholesaleId($id){
        return $this->setData(self::WHOLESALE_ID, $wholesale_id);
    }


    /**
     * Set title
     *
     * @param string $title
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setRuleName($rulename){
        return $this->setData(self::RULENAME, $rulename);
    }
    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setStatus($status){
        return $this->setData(self::STATUS, $status);
    }
     public function setAssignProducts($assign_products){
        return $this->setData(self::ASSIGNPRODUCTS, $assign_products);
    }
     public function setDiscount($discount){
        return $this->setData(self::DISCOUNT, $discount);
    }
     public function setPriority($priority){
        return $this->setData(self::PRIORITY, $priority);
    }
     public function setWebsiteId($websiteid){
        return $this->setData(self::WEBSITEID, $websiteid);
    }
     public function setStoreId($storeid){
        return $this->setData(self::STOREID, $storeid);
    }
    public function setCustomerGroupId($customer_groupid){
        return $this->setData(self::CUSTOMER_GROUPID, $customer_groupid);
    }
}

