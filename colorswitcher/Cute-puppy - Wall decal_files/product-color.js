$mm = jQuery.noConflict();

//<![CDATA[
	window.onload=function(){
		if ($mm("#MyProductColorSwitcher").length > 0){
			var img_count = $mm("#MyProductColorSwitcher").attr("rel");
			if(img_count!=0){
				//canvas
				for(var mm = 1; mm <= img_count; mm++){
					var canvas = document.getElementById('canvas'+mm),
					ctx = canvas.getContext('2d'),
					myimg = document.getElementById('html5image'+mm);
					ctx.drawImage(myimg,0,0);
					var imgd = ctx.getImageData(0, 0, 330, 248),
					pix = imgd.data;
				}
				// BG colour
				var canvas0 = document.getElementById("canvas0"),
				ctx0 = canvas0.getContext("2d"),
				myimg0 = document.getElementById("bgimage");
				ctx0.drawImage(myimg0,0,0);

			}
		}
	}
//]]>

function HTML5ColorSwitch(html5idgroup,html5idcolor,colR,colV,colB,colname,iconlabel){

	var img_count = $mm("#MyProductColorSwitcher").attr("rel");
	if(img_count!=0){
		var canvas = document.getElementById('canvas'+html5idgroup);
		var ctx = canvas.getContext('2d');
		var imgd = ctx.getImageData(0, 0, 330, 248);
		var pix = imgd.data;
		for (var i = 0, n = pix.length; i <n; i += 4) {
		 pix[i] = colR; pix[i+1] = colV; pix[i+2] = colB;
		}

		ctx.putImageData(imgd, 0, 0);
	}

	$mm('[id^=Color_'+html5idgroup+'_]').css('border-color', '#f0f0f0');
	$mm('[id=Color_'+html5idgroup+'_'+html5idcolor+']').css('border-color', '#000000');
	if(html5idgroup == 0){
	}else{
		$mm("#Html5Groupname_"+html5idgroup+"").text(colname);
		$mm("#group_"+html5idgroup+"").val(html5idcolor);
	}

	if(html5idgroup != 0){
		$mm('#product-options-wrapper select').filter(
			function(){
				if($mm(this).attr("rel") == iconlabel){
					var select_id = $mm(this).attr("id");
					var value = $mm('#'+select_id+' option').filter(
						function(){
							return $mm.trim($mm(this).text()) == colname;

						}
					).attr('value');
					/*$mm('#'+select_id+' option').filter(
						function(){
							return $mm.trim($mm(this).text()) == colname;
						}
					).attr('selected','selected');*/
					$mm(this).val(value).change();
					bundle.changeSelection($(select_id));
				}
			}
		);

	}

	canvastoimage(img_count,html5idgroup,colname);
}


function canvastoimage(img_count,html5idgroup,colname){

	if(img_count == 1){
		canvas0 = document.getElementById("canvas0");
		canvas1 = document.getElementById("canvas1");
		var canvas = document.getElementById("Canvasimage");
		var context = canvas.getContext("2d");
		var sources = {
			colorselector0: canvas0.toDataURL("image/png"),
			colorselector1: canvas1.toDataURL("image/png")
		};
		loadImages(sources, function (images) {
			context.drawImage(images.colorselector0, 0, 0, 330, 248);
			context.drawImage(images.colorselector1, 0, 0, 330, 248);
			//finalimage  here which has one canvas data
			var finalimage = document.getElementById("finalimage");
			finalimage.src = canvas.toDataURL("image/png");
			$mm("#canvasimagesrc").attr("value", finalimage.src);

		});
		$mm('select.Color option').filter(
			function(){
				return $mm.trim($mm(this).text()) == colname;
			}
		).attr('selected','selected');

	}

	if(img_count == 2){
		canvas0 = document.getElementById("canvas0");
		canvas1 = document.getElementById("canvas1");
		canvas2 = document.getElementById("canvas2");
		var canvas = document.getElementById("Canvasimage");
		var context = canvas.getContext("2d");
		var sources = {
			colorselector0: canvas0.toDataURL("image/png"),
			colorselector1: canvas1.toDataURL("image/png"),
			colorselector2: canvas2.toDataURL("image/png")
		};
		loadImages(sources, function (images) {
			context.drawImage(images.colorselector0, 0, 0, 330, 248);
			context.drawImage(images.colorselector1, 0, 0, 330, 248);
			context.drawImage(images.colorselector2, 0, 0, 330, 248);
			//finalimage  here which has two canvas data
			var finalimage = document.getElementById("finalimage");
			finalimage.src = canvas.toDataURL("image/png");
			$mm("#canvasimagesrc").attr("value", finalimage.src);
		});
		getselectedoption(html5idgroup,colname);
	}

	if(img_count == 3){
		canvas0 = document.getElementById("canvas0");
		canvas1 = document.getElementById("canvas1");
		canvas2 = document.getElementById("canvas2");
		canvas3 = document.getElementById("canvas3");
		var canvas = document.getElementById("Canvasimage");
		var context = canvas.getContext("2d");
		var sources = {
			colorselector0: canvas0.toDataURL("image/png"),
			colorselector1: canvas1.toDataURL("image/png"),
			colorselector2: canvas2.toDataURL("image/png"),
			colorselector3: canvas3.toDataURL("image/png")
		};
		loadImages(sources, function (images) {
			context.drawImage(images.colorselector0, 0, 0, 330, 248);
			context.drawImage(images.colorselector1, 0, 0, 330, 248);
			context.drawImage(images.colorselector2, 0, 0, 330, 248);
			context.drawImage(images.colorselector3, 0, 0, 330, 248);
			//finalimage  here which has three canvas data
			var finalimage = document.getElementById("finalimage");
			finalimage.src = canvas.toDataURL("image/png");
			$mm("#canvasimagesrc").attr("value", finalimage.src);
		});
		getselectedoption(html5idgroup,colname);
	}

	if(img_count == 4){
		canvas0 = document.getElementById("canvas0");
		canvas1 = document.getElementById("canvas1");
		canvas2 = document.getElementById("canvas2");
		canvas3 = document.getElementById("canvas3");
		canvas4 = document.getElementById("canvas4");
		var canvas = document.getElementById("Canvasimage");
		var context = canvas.getContext("2d");
		var sources = {
			colorselector0: canvas0.toDataURL("image/png"),
			colorselector1: canvas1.toDataURL("image/png"),
			colorselector2: canvas2.toDataURL("image/png"),
			colorselector3: canvas3.toDataURL("image/png"),
			colorselector4: canvas4.toDataURL("image/png")
		};
		loadImages(sources, function (images) {
			context.drawImage(images.colorselector0, 0, 0, 330, 248);
			context.drawImage(images.colorselector1, 0, 0, 330, 248);
			context.drawImage(images.colorselector2, 0, 0, 330, 248);
			context.drawImage(images.colorselector3, 0, 0, 330, 248);
			context.drawImage(images.colorselector4, 0, 0, 330, 248);
			//finalimage  here which has four canvas data
			var finalimage = document.getElementById("finalimage");
			finalimage.src = canvas.toDataURL("image/png");
			$mm("#canvasimagesrc").attr("value", finalimage.src);
		});
		getselectedoption(html5idgroup,colname);
	}

	if(img_count == 5){
		canvas0 = document.getElementById("canvas0");
		canvas1 = document.getElementById("canvas1");
		canvas2 = document.getElementById("canvas2");
		canvas3 = document.getElementById("canvas3");
		canvas4 = document.getElementById("canvas4");
		canvas5 = document.getElementById("canvas5");
		var canvas = document.getElementById("Canvasimage");
		var context = canvas.getContext("2d");
		var sources = {
			colorselector0: canvas0.toDataURL("image/png"),
			colorselector1: canvas1.toDataURL("image/png"),
			colorselector2: canvas2.toDataURL("image/png"),
			colorselector3: canvas3.toDataURL("image/png"),
			colorselector4: canvas4.toDataURL("image/png"),
			colorselector5: canvas5.toDataURL("image/png")
		};
		loadImages(sources, function (images) {
			context.drawImage(images.colorselector0, 0, 0, 330, 248);
			context.drawImage(images.colorselector1, 0, 0, 330, 248);
			context.drawImage(images.colorselector2, 0, 0, 330, 248);
			context.drawImage(images.colorselector3, 0, 0, 330, 248);
			context.drawImage(images.colorselector4, 0, 0, 330, 248);
			context.drawImage(images.colorselector5, 0, 0, 330, 248);
			//finalimage  here which has five canvas data
			var finalimage = document.getElementById("finalimage");
			finalimage.src = canvas.toDataURL("image/png");
			$mm("#canvasimagesrc").attr("value", finalimage.src);
		});
		getselectedoption(html5idgroup,colname);
	}

	if(img_count == 6){
		canvas0 = document.getElementById("canvas0");
		canvas1 = document.getElementById("canvas1");
		canvas2 = document.getElementById("canvas2");
		canvas3 = document.getElementById("canvas3");
		canvas4 = document.getElementById("canvas4");
		canvas5 = document.getElementById("canvas5");
		canvas6 = document.getElementById("canvas6");
		var canvas = document.getElementById("Canvasimage");
		var context = canvas.getContext("2d");
		var sources = {
			colorselector0: canvas0.toDataURL("image/png"),
			colorselector1: canvas1.toDataURL("image/png"),
			colorselector2: canvas2.toDataURL("image/png"),
			colorselector3: canvas3.toDataURL("image/png"),
			colorselector4: canvas4.toDataURL("image/png"),
			colorselector5: canvas5.toDataURL("image/png"),
			colorselector6: canvas6.toDataURL("image/png")
		};
		loadImages(sources, function (images) {
			context.drawImage(images.colorselector0, 0, 0, 330, 248);
			context.drawImage(images.colorselector1, 0, 0, 330, 248);
			context.drawImage(images.colorselector2, 0, 0, 330, 248);
			context.drawImage(images.colorselector3, 0, 0, 330, 248);
			context.drawImage(images.colorselector4, 0, 0, 330, 248);
			context.drawImage(images.colorselector5, 0, 0, 330, 248);
			context.drawImage(images.colorselector6, 0, 0, 330, 248);
			//finalimage  here which has six canvas data
			var finalimage = document.getElementById("finalimage");
			finalimage.src = canvas.toDataURL("image/png");
			$mm("#canvasimagesrc").attr("value", finalimage.src);
		});
		getselectedoption(html5idgroup,colname);
	}
}

function loadImages(sources, callback) {
	var images = {};
	var loadedImages = 0;
	var numImages = 0;
	// get num of sources
	for (var src in sources) {
		numImages++;
	}
	for (var src in sources) {
		images[src] = new Image();
		images[src].onload = function () {
			if (++loadedImages >= numImages) {
				callback(images);
			}
		};
		images[src].src = sources[src];
	}
}


function flipAround()
{
	var img_count = $mm("#MyProductColorSwitcher").attr("rel");
	for(var mm = 1; mm <= img_count; mm++){
		var canvas = document.getElementById('canvas'+mm);
		ctx = canvas.getContext('2d');
		var imgd = ctx.getImageData(0, 0, 330, 248);
		// Traverse every row and flip the pixels
		for (i=0; i<imgd.height; i++)
		{
			// We only need to do half of every row since we're flipping the halves
			for (j=0; j<imgd.width/2; j++)
			{
				var index=(i*4)*imgd.width+(j*4);
				var mirrorIndex=((i+1)*4)*imgd.width-((j+1)*4);
				for (p=0; p<4; p++)
				{
					var temp=imgd.data[index+p];
					imgd.data[index+p]=imgd.data[mirrorIndex+p];
					imgd.data[mirrorIndex+p]=temp;
				}
			}
		}
		ctx.putImageData(imgd,0,0,0,0, 330, 248);
	}
	canvastoimage(img_count);
}

function getselectedoption(html5idgroup,colname){

	$mm('select.Color'+html5idgroup+' option').filter(
		function(){
			return $mm.trim($mm(this).text()).search(colname)>-1 ? true : false;
		}
	).attr('selected','selected');
}

$mm(document).ready(function() {

	var img_count = $mm("#MyProductColorSwitcher").attr("rel");
console.log($mm("#MyProductColorSwitcher"));
	if(img_count <= 1){

		$mm("select.Color").change(function() {
			var bund_pro_name = $mm("option:selected", this).attr("class");

			$mm('#Html5Group_1_colors div').filter(
				function(){
					if($mm.trim(bund_pro_name).search($mm(this).attr('title'))>-1){
						return $mm(this).attr('onclick');
					}
				}
			).trigger('onclick');
		});
	}else{
		$mm("select.bundle-option-select").change(function() {
			var bund_pro_name = $mm("option:selected", this).attr("class");
			var rel = $mm(this).attr("rel");
			if (rel.indexOf("Color") > -1) {
				if($mm(".Html5GroupColors").hasClass(rel)){
					var html5id = $mm('.'+rel).attr("id");
					$mm('#'+html5id+' div').filter(
						function(){
							if($mm.trim(bund_pro_name).search($mm(this).attr('title'))>-1){
								return $mm(this).attr('onclick');
							}
						}
					).trigger('onclick');
				}
			}
		});
	}

	$mm("select.Position").change(function() {
		flipAround();
	});
});



