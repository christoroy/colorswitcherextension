<?php
namespace Ewall\Wholesaleprice\Model\Grid\Source;

class Status implements \Magento\Framework\Data\OptionSourceInterface
{
  /**
  * @var \Magebuzz\Staff\Model\Grid
  */
  protected $_grid;

  /**
  * Constructor
  *
  * @param \Magebuzz\Staff\Model\Grid $grid
  */
  public function __construct(\Ewall\Wholesaleprice\Model\Grid $grid)
  {
      $this->_grid = $grid;
  }

  /**
  * Get options
  *
  * @return array
  */
  public function toOptionArray()
  {
      $options[] = ['label' => '', 'value' => ''];
      $availableOptions = $this->_grid->getAvailableStatuses();
      foreach ($availableOptions as $key => $value) {
          $options[] = [
          'label' => $value,
          'value' => $key,
          ];
  }
  return $options;
  }
}