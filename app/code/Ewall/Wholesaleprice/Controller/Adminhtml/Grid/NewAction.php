<?php
 
namespace Ewall\Wholesaleprice\Controller\Adminhtml\Grid;
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */
 
class NewAction extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\Forward
     */
    protected $resultForwardFactory;
 
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }
 
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ewall_Wholesaleprice::save');
    }
 
    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $this->_initItem(true);
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }

    protected function _initItem($getRootInstead = false)
    {
        $id = (int)$this->getRequest()->getParam('wholesale_id', false);
        $myModel = $this->_objectManager->create('Ewall\Wholesaleprice\Model\Grid');

        if ($id) {
            $myModel->load($id);            
        }
        $this->_objectManager->get('Magento\Framework\Registry')->unregister('grid');
        $this->_objectManager->get('Magento\Framework\Registry')->register('grid', $myModel);
        $this->_objectManager->get('Magento\Cms\Model\Wysiwyg\Config');
        return $myModel;
    }  
}
