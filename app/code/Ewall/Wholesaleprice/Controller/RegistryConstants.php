<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Ewall\Wholesaleprice\Controller;

/**
 * Declarations of core registry keys used by the Wholesaleprice module
 *
 */
class RegistryConstants
{
    /**
     * Key for current catalog rule in registry
     */
    const CURRENT_WHOLESALEPRICE_WHOLESALE_ID = 'current_wholesaleprice';
}
