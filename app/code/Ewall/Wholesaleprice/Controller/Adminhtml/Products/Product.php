<?php
namespace Ewall\Wholesaleprice\Controller\Adminhtml\Products;


abstract class Product extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Ewall_Wholesaleprice::grid_list';
}
?>