<?php
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */

namespace Ewall\Wholesaleprice\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Save extends \Magento\Backend\App\Action
{

    protected $collection;

    protected $date;

    protected $WholesaleproductsFactory;

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Ewall\Wholesaleprice\Model\Grid $collection,
        \Ewall\Wholesaleprice\Model\WholesaleproductsFactory $WholesaleproductsFactory,
        Action\Context $context

    )
    {
        $this->_collection = $collection;
        $this->_WholesaleproductsFactory = $WholesaleproductsFactory;
        $this->_date = $date;
        parent::__construct($context);
       }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ewall_Wholesaleprice::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        // print_r($data);exit;

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            // $this->_collection = $this->_objectManager->create('Ewall\Wholesaleprice\Model\Grid');
            $id = $this->getRequest()->getParam('wholesale_id');
            if ($id) {
                $this->_collection->load($id);
            }else{
            $this->_collection->setCreatedTime($this->_date->gmtDate());
            }
            $data_websiteid = implode(",",$data['websiteid']);
            $data_customer_groupid = implode(",",$data['customer_groupid']);

            if (isset($data['category_ids']) && $data['category_ids'] != 'false') {
                $data_category_ids = serialize($data['category_ids']);
            } else {
                $data_category_ids = null;
            }

            if (isset($data['category_products'])) {
                // $data_category_products1 = implode(",",array_keys(json_decode($data['category_products'], true)));
                $data_category_products = serialize(json_decode($data['category_products'], true));
                // print_r($data_category_products); exit;
            }else {
                $data_category_products = "";
            }
            // if (null != serialize(array_keys(json_decode($data['category_products'], true)))) {
            // }
            // echo "<pre>";print_r($data_category_products);exit();
            // echo "<pre>";print_r($this->_collection->getCategoryIds());exit();
            $this->_collection->setRuleName($data['rulename']);
            $this->_collection->setStatus($data['status']);
            $this->_collection->setWebsiteId($data_websiteid);
            $this->_collection->setStoreId($data['storeid']);
            $this->_collection->setCustomerGroupId($data_customer_groupid);
            $this->_collection->setDiscount($data['discount']);
            $this->_collection->setCategoryDiscount($data['category_discount']);
            $this->_collection->setPriority($data['priority']);
            $this->_collection->setCategoryIds($data_category_ids);
            $this->_collection->setAssignProducts($data_category_products);
            $this->_collection->setUpdatedTime($this->_date->gmtDate());

            try {
                $this->_collection->save();
                $discountProducts = json_decode($data['category_products'], true);
                // $discountProducts = array('2055' => '45', '2054' => '25', '2053' => '45');
                $rule_id = $this->_collection->getWholesaleId();
                if ($this->_WholesaleproductsFactory->create()->getCollection()->addFieldToFilter('rule_id', array('eq' => $rule_id))->getData())
                {
                    $Wholesaleproducts = $this->_WholesaleproductsFactory->create();
                    $Wholesaleproducts->getCollection()->addFieldToFilter('rule_id', array('eq' => $rule_id))->walk('delete');
                    // print_r($Wholesaleproducts->getCollection()->addFieldToFilter('rule_id', array('eq' => $rule_id))->getData());exit;
                }
                foreach ($discountProducts as $key => $discount) {
                    $Wholesaleproducts = $this->_WholesaleproductsFactory->create();
                    $Wholesaleproducts->setProductId($key);
                    $Wholesaleproducts->setRuleId($rule_id);
                    $Wholesaleproducts->setDiscount($discount);
                    $Wholesaleproducts->save();
                }
                $this->messageManager->addSuccess(__('You saved '));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['wholesale_id' => $this->_collection->getWholesaleId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['wholesale_id' => $this->getRequest()->getParam('wholesale_id')]);
         }
        return $resultRedirect->setPath('*/*/');
    }
}