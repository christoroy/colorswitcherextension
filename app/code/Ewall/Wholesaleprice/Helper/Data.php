<?php


namespace Ewall\Wholesaleprice\Helper;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $storeManager;

    protected $objectManager;

    const XML_PATH_WHOLESALEPRICE = 'wholesaleprice/';

    public function __construct(Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager  = $storeManager;
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }


    // public function getGeneralConfig($code, $storeId = null)
    // {
    //     return $this->getConfigValue(self::XML_PATH_WHOLESALEPRICE . $code, $storeId);
    // }

}
