<?php

namespace Ewall\Wholesaleprice\Model\Rewrite;

class Price extends \Magento\Catalog\Model\Product\Type\Price
{
  	protected $_modelGridFactory;

  	protected $_wholesaleProducts;

  	protected $_productCollectionFactory;

  	protected $_categoryFactory;

  	protected $_registry;

  	protected $_customerSession;

  	protected $_storeManager;

  	protected $_helper;

  	public function __construct(
        \Ewall\Wholesaleprice\Model\GridFactory $gridFactory,
        \Ewall\Wholesaleprice\Helper\Data $helper,
        \Ewall\Wholesaleprice\Model\Wholesaleproducts $wholesaleProducts,
        \Magento\Customer\Api\GroupManagementInterface $groupManagement,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Event\Observer $observer,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
        ) {

        	$this->_groupManagement = $groupManagement;
            $this->_modelGridFactory = $gridFactory;
            $this->_wholesaleProducts = $wholesaleProducts;
            $this->_productCollectionFactory = $productCollectionFactory;
            $this->_categoryFactory = $categoryFactory;
            $this->_registry = $registry;
            $this->_observer = $observer;
            $this->_customerSession = $customerSession;
            $this->_storeManager = $storeManager;
            $this->_helper = $helper;
    }

    public function getPrice($product)
    {
    	$entity_id = $product->getData('entity_id');
    	$price = $product->getData('price');
    	$wholesaleprice_cofig_status = $this->_helper->getConfigValue('wholesaleprice/general/enable');
	    if ($wholesaleprice_cofig_status != '1') {
	      	return $price;
	    }
	    $current_customer_group = $this->_customerSession->getCustomer()->getGroupId();
	    $current_store_id = $this->_storeManager->getStore()->getId();
	    $current_website_id = $this->_storeManager->getStore()->getWebsiteId();
	    $gridCollection = $this->_modelGridFactory->create()->getCollection();

	    foreach ($gridCollection->setOrder('priority','DESC') as $gridData) {
	      	if ($status = $gridData->getStatus()) {
		        $store_id = $gridData->getStoreid();
		        $website_id = $gridData->getWebsiteid();
		        $customer_group = explode(',', $gridData->getCustomerGroupid());
		        $priority = $gridData->getPriority();
		        $general_discount = $gridData->getDiscount();
		        $category_discount = $gridData->getCategoryDiscount();
		        $current_categories = unserialize($gridData->getCategoryIds()) ? $current_categories = unserialize($gridData->getCategoryIds()) : $current_categories = null;
		        $assign_products = unserialize($gridData->getAssignProducts()) ? $assign_products = unserialize($gridData->getAssignProducts()) : $assign_products = null;
	      	}else {
		        $store_id = null;
		        $website_id = null;
		        $customer_group = null;
		        $priority = null;
		        $general_discount = null;
		        $category_discount = null;
		        $current_categories = null;
		        $assign_products = null;
	      	}
	    }

	    if (isset($priority) && in_array($current_customer_group, $customer_group) && $current_store_id == $store_id && $current_website_id == $website_id) {
	      	isset($assign_products) ? $assign_product_id = array_keys($assign_products) : $assign_product_id = array();
	    }else {
	      	return $price;
	    }
	    if (isset($current_categories)) {
	      	foreach ($current_categories as $categoryData) {
		        $category = $this->_categoryFactory->create()->load($categoryData);
		        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoryFilter($category)->load()->getData();
		        foreach ($collection as $value) {
		          	$category_product_ids[] = $value['entity_id'];
		        }
	      	}
	    }else {
	      	$category_product_ids = array();
	    }

	    if (in_array($entity_id, $assign_product_id)) {
        	$discount = $price-round(($price/100) * $assign_products[$entity_id]);
	    }elseif (in_array($entity_id, $category_product_ids)) {
	    	$discount = $price-round(($price/100) * $category_discount);
	    }else {
	    	$discount = $price-round(($price/100) * $general_discount);
	    }
        return $discount;
    }

    public function getAllCustomerGroupsId()
    {
        // ex: 32000
        return $this->_groupManagement->getAllCustomersGroup()->getId();
    }

}