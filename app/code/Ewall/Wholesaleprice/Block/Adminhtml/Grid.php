<?php
namespace Ewall\Wholesaleprice\Block\Adminhtml;

class Grid extends \Magento\Backend\Block\Widget\Grid\Container
{
  protected function _construct()
  {
    $this->_controller = 'adminhtml_grid';
    $this->_blockGroup = 'Ewall_Wholesaleprice';
    $this->_headerText = __('Manage Wholesale Price Rule');

    parent::_construct();

    if ($this->_isAllowedAction('Ewall_Wholesaleprice::save')) {
    $this->buttonList->update('add', 'label', __('Add NewRule'));
    } else {
    $this->buttonList->remove('add');
    }
  }

  protected function _isAllowedAction($resourceId)
  {
    return $this->_authorization->isAllowed($resourceId);
  }
}