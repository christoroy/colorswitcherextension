<?php
namespace Ewall\Wholesaleprice\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class ListPageProductPrice implements ObserverInterface
{
  protected $_modelGridFactory;
  protected $_productCollectionFactory;
  protected $_productloader;
  protected $_categoryFactory;
  protected $_storeManager;
  protected $_customerSession;
  public function __construct(
        \Ewall\Wholesaleprice\Model\GridFactory $gridFactory,
    	\Ewall\Wholesaleprice\Helper\Data $helper,
    	\Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ProductFactory $_productloader
        ) {
        $this->_modelGridFactory = $gridFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productloader = $_productloader;
        $this->_categoryFactory = $categoryFactory;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->_helper = $helper;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
	    $gridCollection = $this->_modelGridFactory->create()->getCollection();
		foreach ($gridCollection->setOrder('priority','DESC') as $gridData) {
		    if ($status= $gridData->getStatus()) {
		        $store_id = $gridData->getStoreid();
		        $website_id = $gridData->getWebsiteid();
		        $customer_group = explode(',', $gridData->getCustomerGroupid());
		        $priority = $gridData->getPriority();
		        $general_discount = $gridData->getDiscount();
		        $category_discount = $gridData->getCategoryDiscount();
		        $current_categories = unserialize($gridData->getCategoryIds()) ? $current_categories = unserialize($gridData->getCategoryIds()) : $current_categories = null;
	        	$assign_products = unserialize($gridData->getAssignProducts()) ? $assign_products = unserialize($gridData->getAssignProducts()) : $assign_products = null;
		    }else {
		        $store_id = null;
		        $website_id = null;
		        $customer_group = null;
		      	$general_discount = null;
		        $category_discount = null;
		        $current_categories = null;
		        $assign_products = null;
		        $priority = null;
		    }
		}

	    $current_customer_group = $this->_customerSession->getCustomer()->getGroupId();
	    $current_store_id = $this->_storeManager->getStore()->getId();
	    $current_website_id = $this->_storeManager->getStore()->getWebsiteId();
	    $wholesaleprice_cofig_status = $this->_helper->getConfigValue('wholesaleprice/general/enable');

	    if ($wholesaleprice_cofig_status == '1' && isset($priority) && in_array($current_customer_group, $customer_group) && $current_store_id == $store_id && $current_website_id == $website_id) {
	    	isset($assign_products) ? $assign_product_id = array_keys($assign_products) : $assign_product_id = array();
		    // print_r($current_categories);exit;
		    if (isset($current_categories)) {
		      foreach ($current_categories as $categoryData) {
		        $category = $this->_categoryFactory->create()->load($categoryData);
		        // echo "$categoryData";exit;
	        	// print_r($category->getData());exit;
	        	$collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoryFilter($category)->getData();
		        // $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*')->addCategoryFilter($category)->load()->getData();
	        	// print_r($collection);exit;
		        foreach ($collection as $value) {
		          // print_r($value); exit;
		          $category_product_ids[] = $value['entity_id'];
		        }
		      }
		    }

	      	$products = $observer->getEvent()->getCollection();
	      	$product = $this->_productloader->create()->getCollection()->addAttributeToSelect('*')->getData();
	      	foreach ($products as $productData) {
		      	$entity_id = $productData->getData('entity_id');
			    $price = $productData->getData('price');
		        if (in_array($entity_id, $assign_product_id)) {
		        // print_r($assign_products[$entity_id]);exit;
				    $discount_price = $price-round(($price / 100) * $assign_products[$entity_id]);
			        // $productData->setSpecialPrice($discount_price);
		        }elseif (in_array($entity_id, $category_product_ids)){
		    // print_r($category_product_ids);exit;
				    $discount_price = $price-round(($price / 100) * $category_discount);
			        // $productData->setSpecialPrice($discount_price);
		        }else {
		        	$discount_price = $price-round(($price / 100) * $general_discount);
			        // $productData->setSpecialPrice($discount_price);
		        }
	      	}
	    }
    }
}

